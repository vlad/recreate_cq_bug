package com.example.demo;
import org.springframework.boot.*;
import org.springframework.boot.autoconfigure.*;
import org.springframework.web.bind.annotation.*;

@SpringBootApplication
@RestController
public class DemoApplication {

@GetMapping("/")
String home() {
return "Spring is here!";
}

public static void main(String[] args) {
System.out.println("Now I am in Main!");


SpringApplication.run(DemoApplication.class, args);
}
public int foo(int a) {
int b = 12;
if (a == 1) {
return b;
}
return b; // Noncompliant
}
public int foo1(int a) {
int b = 12;
if (a == 1) {
return b;
}
return b; // Noncompliant
}
}